package sample.dto;

public class User {

    private String id;
    private String nick;

    public User(String id, String name) {
        this.id = id;
        this.nick = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!id.equals(user.id)) return false;
        return nick.equals(user.nick);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + nick.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", nick='" + nick + '\'' +
                '}';
    }
}
