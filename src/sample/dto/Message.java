package sample.dto;

public class Message {

    private String id;
    private String nick;
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message(String id, String nick, String message) {
        this.id = id;
        this.nick = nick;
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message user = (Message) o;

        if (!id.equals(user.id)) return false;
        if (!nick.equals(user.nick)) return false;
        return message != null ? message.equals(user.message) : user.message == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + nick.hashCode();
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", nick='" + nick + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}