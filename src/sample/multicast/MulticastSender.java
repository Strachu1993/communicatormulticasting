package sample.multicast;

import sample.config.Config;
import java.io.IOException;
import java.net.*;

public class MulticastSender {

    private InetAddress ia = null;
    private int port = 0;

    public MulticastSender(){
        initPort();
        initIp();
    }

    private void initPort(){
        port = Config.port;
    }

    private void initIp(){
        try {
            ia = InetAddress.getByName(Config.ip);
        }catch(UnknownHostException e){
            System.err.println("UnknownHostException");
        }
    }

    public void send(String text){
        byte[] data = text.getBytes();

        DatagramPacket dp = new DatagramPacket(data, data.length, ia, port);
        try {
            MulticastSocket ms = new MulticastSocket();
            ms.joinGroup(ia);

            ms.setTimeToLive(Config.routerCount);
            ms.send(dp);

            ms.leaveGroup(ia);
            ms.close();
        } catch (SocketException se) {
            System.err.println(se);
        } catch (IOException ie) {
            System.err.println(ie);
        }
    }

}