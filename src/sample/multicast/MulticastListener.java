package sample.multicast;

import sample.config.Config;
import sample.controller.Controller;
import java.io.IOException;
import java.net.*;

public class MulticastListener implements Runnable {

    private InetAddress inetAddress = null;
    private byte[] buffer = null;
    private DatagramPacket datagramPacket = null;
    private Controller controller = null;
    private int port = 0;

    public MulticastListener(Controller controller){
        this.controller = controller;
        initDatagramAndBuffer();
        initPort();
        initIp();
        new Thread(this).start();
    }

    @Override
    public void run() {
        listener();
    }

    private void initDatagramAndBuffer(){
        buffer = new byte[65509];
        datagramPacket = new DatagramPacket(buffer, buffer.length);
    }

    private void initPort(){
        port = Config.port;
    }

    private void initIp(){
        try {
            inetAddress = InetAddress.getByName(Config.ip);
        } catch (UnknownHostException e) {
            System.exit(1);
        }
    }

    private void listener(){
        try {
            MulticastSocket ms = joinToGroup();
            while (!Config.exit) {
                ms.receive(datagramPacket);
                String response = new String(datagramPacket.getData(), 0, datagramPacket.getLength());

                if(response.substring(0, 4).equals("add:"))
                    controller.addToListView(response.substring(4));
                else
                    controller.addToTextArea(response);
            }
        } catch (SocketException se) {
            System.err.println(se);
        } catch (IOException ie) {
            System.err.println(ie);
        }
    }

    private MulticastSocket joinToGroup() throws IOException {
        MulticastSocket ms = new MulticastSocket(port);
        ms.joinGroup(inetAddress);
        return ms;
    }

}