package sample.config;

import java.util.UUID;

public class Config {

    public static final int port = 1456;

    public static final String ip = "224.2.255.24";

    public static final int routerCount = 1;

    public static String nick;

    public static final String uniqueId = UUID.randomUUID().toString();

    public static boolean exit = false;
}
