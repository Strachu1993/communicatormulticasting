package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import sample.config.Config;
import sample.dto.Message;
import sample.dto.User;
import sample.multicast.MulticastListener;
import sample.multicast.MulticastSender;
import sample.fxml.NewListView;
import java.net.URL;
import java.util.*;

public class Controller implements Initializable {

    private MulticastListener multicastSniffer = null;
    private MulticastSender multicastSender = null;

    @FXML
    private Button buttonSend;

    @FXML
    private Button buttonConnect;

    @FXML
    private NewListView usersList;

    @FXML
    private TextArea messagesArea;

    @FXML
    private TextField fieldMessage;

    @FXML
    private TextField fieldNick;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        messagesArea.setEditable(false);
        buttonSend.setVisible(false);
    }

    @FXML
    public void clickButtonSend() {
        if(checkIsNotNullAndNotEmpty(fieldMessage.getText())) {
            multicastSender.send( Config.uniqueId + ":" +Config.nick + ":" + fieldMessage.getText());
            fieldMessage.setText("");
        }
    }

    @FXML
    public void clickButtonConnect(){
        if(checkIsNotNullAndNotEmpty(fieldNick.getText())) {
            createConnect();
        }
    }

    private void createConnect() {
        multicastSniffer = new MulticastListener(this);
        multicastSender = new MulticastSender();
        setInitNick();
        sendToAllAmI();
        prepareButtons();
    }

    private void setInitNick() {
        Config.nick = fieldNick.getText();
    }

    private void sendToAllAmI(){
        multicastSender.send("add:" + Config.uniqueId + ":" +Config.nick);
    }

    private void prepareButtons() {
        buttonSend.setVisible(true);
        buttonConnect.setVisible(false);
        fieldNick.setEditable(false);
    }

    public void addToTextArea(String message) {
        Message mess = parserMessage(message);
        addMessageToTextArea(mess.getNick() + ": " + mess.getMessage() + "\n");
        addUserToList(mess.getId() + ":" + mess.getNick());
    }

    public void addToListView(String message){
        User user = addUserToList(message);

        if(!Config.uniqueId.equals(user.getId()) && !usersList.isExistsIdInList(user.getId())) {
            addMessageToTextArea(user.getNick() + " -> dołączył do czatu" + "\n");
            usersList.addToList(new User(user.getId(), user.getNick()));
            sendToAllAmI();
        }
    }

    private User addUserToList(String message) {
        User user = parserMessageAdd(message);
        return user;
    }

    private void addMessageToTextArea(String text) {
        messagesArea.appendText(text);
        messagesArea.setScrollTop(messagesArea.getLength());
    }

    private Message parserMessage(String message) {
        int i = message.indexOf(':');
        int ii = message.lastIndexOf(':');
        return new Message(message.substring(0, i), message.substring(i+1, ii) , message.substring(ii+1));
    }

    private User parserMessageAdd(String message) {
        int i = message.indexOf(':');
        return new User(message.substring(0, i), message.substring(i+1));
    }

    private boolean checkIsNotNullAndNotEmpty(String text){
        return !("".equals(text) && text != null);
    }

}
