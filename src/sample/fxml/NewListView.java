package sample.fxml;

import javafx.application.Platform;
import javafx.scene.control.ListView;
import sample.dto.User;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class NewListView extends ListView<String> {

    private List<User> users = null;

    public NewListView(){
        super();
        users = new ArrayList<>();
    }

    public void addToList(User user){
        if(!isExistsIdInList(user.getId()) && !checkIsNickExist(user.getNick())){
            Platform.runLater(() -> {
                        getItems().addAll(user.getNick());
                    });
            users.add(user);
        }
    }

    public boolean isExistsIdInList(String uuid) {
        return users.stream()
                .map(User::getId)
                .filter(Predicate.isEqual(uuid))
                .count() > 0;
    }

    private boolean checkIsNickExist(String name){
        return getItems().contains(name);
    }

}
